<!DOCTYPE html>
<html>
<body>

<h1>html 넣기</h1>
<textarea id="source" style="width:100%;height:200px;"></textarea>
<button id="create">변환하기</button>
<h1>친구사이 홈페이지용</h1>
<textarea id="chingusai" style="width:100%;height:200px;" readonly></textarea>
<h1>이반시티용</h1>
<textarea id="ivancity" style="width:100%;height:200px;" readonly></textarea>

<script src="https://chingusai.net/xe/common/js/jquery.js"></script>
<script>
$("button").click(function() {
	var src = $("#source").val();
	var f = src.indexOf("<table");
	var l = src.lastIndexOf("</table>");
	var inner = src.substr(f, l+8-f).replace(/<!--[\s\S]*?-->/g,'');
	$("#chingusai").val("<div id='CHINGUSAI_NEWSLETTER'>"+inner+"</div>");
	$("#ivancity").val("<div><style scoped>#CHINGUSAI_NEWSLETTER .MsoNormal,#CHINGUSAI_NEWSLETTER .display-button a,#CHINGUSAI_NEWSLETTER .display-button td,#CHINGUSAI_NEWSLETTER a{font-family:'Open Sans',Arial,Helvetica Neue,Helvetica,sans-serif!important}#CHINGUSAI_NEWSLETTER img{display:block!important;border:0;-ms-interpolation-mode:bicubic}#CHINGUSAI_NEWSLETTER .ExternalClass,#CHINGUSAI_NEWSLETTER .ReadMsgBody{width:100%}#CHINGUSAI_NEWSLETTER .ExternalClass,#CHINGUSAI_NEWSLETTER .ExternalClass div,#CHINGUSAI_NEWSLETTER .ExternalClass font,#CHINGUSAI_NEWSLETTER .ExternalClass p,#CHINGUSAI_NEWSLETTER .ExternalClass span,#CHINGUSAI_NEWSLETTER .ExternalClass td{line-height:100%}#CHINGUSAI_NEWSLETTER .images{display:block!important;width:100%!important}#CHINGUSAI_NEWSLETTER p{margin:0!important;padding:0!important}#CHINGUSAI_NEWSLETTER .display-button a:hover{text-decoration:none!important}#CHINGUSAI_NEWSLETTER a,#CHINGUSAI_NEWSLETTER a:active,#CHINGUSAI_NEWSLETTER a:hover,#CHINGUSAI_NEWSLETTER a:visited{color:inherit!important;text-decoration:none!important;font-size:inherit!important}#CHINGUSAI_NEWSLETTER .display-width,#CHINGUSAI_NEWSLETTER .space,#CHINGUSAI_NEWSLETTER .space1,#CHINGUSAI_NEWSLETTER table[class=display-width-child],#CHINGUSAI_NEWSLETTER table[class=display-width-inner],#CHINGUSAI_NEWSLETTER table[class=display-width]{width:100%!important}#CHINGUSAI_NEWSLETTER .res-padding,#CHINGUSAI_NEWSLETTER .res-padding1{padding:0 30px!important}#CHINGUSAI_NEWSLETTER .resp-pad-left{padding:0!important}#CHINGUSAI_NEWSLETTER .height50{height:50px!important}#CHINGUSAI_NEWSLETTER .height-hidden,#CHINGUSAI_NEWSLETTER .height-hidden1,#CHINGUSAI_NEWSLETTER .height-hidden2{display:none!important}#CHINGUSAI_NEWSLETTER .client{width:280px!important}#CHINGUSAI_NEWSLETTER .text-center{text-align:center!important}#CHINGUSAI_NEWSLETTER .image-center{width:62px!important;margin:0 auto!important;display:block}@media only screen and (max-width:480px){#CHINGUSAI_NEWSLETTER table[class=display-width] table{width:100%!important}#CHINGUSAI_NEWSLETTER table[class=display-width] .button-width .display-button,#CHINGUSAI_NEWSLETTER table[class=display-width] .client{width:auto!important}#CHINGUSAI_NEWSLETTER table[class=display-width] .quote{width:180px!important}#CHINGUSAI_NEWSLETTER table[class=display-width] .launch{font-size:45px!important}}@media only screen and (max-width:360px){#CHINGUSAI_NEWSLETTER table[class=display-width] .menu{font-size:10px!important}#CHINGUSAI_NEWSLETTER table[class=display-width] .client{width:auto!important}#CHINGUSAI_NEWSLETTER img[class=client-img]{width:100%!important;height:auto!important}}@media only screen and (max-width:300px){#CHINGUSAI_NEWSLETTER table[class=display-width] .menu{font-size:9px!important}}</style><div id='CHINGUSAI_NEWSLETTER'>"+inner.replace(/\>[ \t\n]+/g,'>').replace(/[ \t\n]+\</g,'<')+"</div></div>");

});

</script>
</body>
</html>

