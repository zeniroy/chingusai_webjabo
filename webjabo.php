<?php
	session_start();
	include 'config.php';
	if(!isset($_SESSION['check_auth'])) {
		if($_POST['password'] && $_POST['password'] == $SERVICE_PASSWORD) {
			$_SESSION['check_auth'] = true;
		}
	}
	else {
		if(!empty($_FILES)) {
			if($USE_LOCAL_UPLOAD) {
				$uploadfile = $LOCAL_UPLOAD_DIR . basename($_FILES['img']['name']);

				move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile);
				$uploaded_file = $LOCAL_UPLOAD_ADDR . basename($_FILES['img']['name']);
			} else {
				$conn_id = ftp_connect($FTP_SERVER_ADDR);
				$login_result = ftp_login($conn_id, $FTP_USERNAME, $FTP_PASSWORD); 

				if (ftp_put($conn_id, $FTP_UPLOAD_DIR.'/'.$_FILES['img']['name'], $_FILES['img']['tmp_name'], FTP_ASCII)) { 
					$uploaded_file = $FTP_UPLOAD_ADDR.'/'.$_FILES['img']['name']; 
					ftp_close($conn_id); 
				} else { 
					echo "There was a problem while uploading $file\n"; 
					ftp_close($conn_id); 
					exit; 
				} 
			}
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
<title>웹자보 만들기</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<style>
		.linkBox {position:absolute !important;top:0;left:0;background-color:yellow;width:100px;height:100px;-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";filter: alpha(opacity=50);-moz-opacity: 0.5;-khtml-opacity: 0.5;opacity: 0.5;word-wrap:break-word;}
	</style>
</head>
<body>
<?php
	if(isset($_SESSION['check_auth'])) {
		if(isset($uploaded_file)) {
			?>
				<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
					<div class="container">
						<div class="navbar-header" style="padding-top:8px;">
							<?=$uploaded_file?>
							<button class="btn btn-default" id="AddLink"><span class="glyphicon glyphicon-plus"></span> 링크 추가하기</button>
							<button class="btn btn-default" id="Complete" data-toggle="modal" data-target="#completeModal">완료</button>
						</div>
					</div>
				</nav>
				<div class="container" style="padding-top:80px;padding-bottom:40px;">
					<div class="row">
						<div style="position:relative;float:left;border:3px solid black;" id="ImageBox">
							<img src="<?= $uploaded_file ?>"/>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
				<div class="modal fade" id="myModal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">이동할 링크에 대한 정보를 입력해주세요</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="Link">이동할 링크를 입력해주세요</label>
									<input type="text" class="form-control" id="Link"></input>
								</div>
								<div class="form-group">
									<label for="Title">시각장애인들에게 이 링크를 설명해주세요</label>
									<input type="text" class="form-control" id="Title"></input>
								</div>

							</div>
							<div class="modal-footer">
								<button id="DeleteBtn" type="button" class="btn btn-danger" data-dismiss="modal">삭제하기</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">취소하기</button>
								<button id="SaveBtn" type="button" class="btn btn-primary" data-dismiss="modal">저장하기</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<div class="modal fade" id="completeModal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">다음 코드를 사용하세요</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="forHomepage">홈페이지용 코드</label>
									<textarea class="form-control" rows="10" readonly="readonly" id="forHomepage"></textarea>
									<br>
									<label for="forEmail">이메일용 코드</label>
									<textarea class="form-control" rows="10" readonly="readonly" id="forEmail"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">창닫기</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			<?
		} else {
			?>
			<div style="padding:40px;">
				<h1>그림 파일을 업로드해 주세요</h1>
				<h3>같은 이름의 파일을 두번 업로드하면 이전 것은 지워집니다.</h3>
				<form enctype="multipart/form-data" method="POST" role="form">
					<div class="form-group">
						<input type="file" name="img"></input>
					</div>
					<button type="submit" class="btn btn-default" id="FileUploadBtn">업로드하기</button>
				</form>
			</div>
			<?
		}
	} else {
		?>
			<div style="padding:40px;">
				<h1>비밀번호를 넣어주세요</h1>
				<form method="POST" role="form">
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password"></input>
					</div>
				</form>
			</div>
		<?
	}
?>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="//code.jquery.com/jquery-1.9.1.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="//raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var initMsg = "박스를 눌러 이동할 링크를 입력해주세요";
		var activeBox = null;
		$("#AddLink").click(function(){
			$('<a href="#" class="linkBox" data-toggle="modal" data-target="#myModal"><span>'+initMsg+'</span></a>').css("top", $(window).scrollTop()).draggable().resizable().click(function(){
				if($(this).find("> span").html()!==initMsg) {
					$("#myModal input:first").val($(this).find("> span").html());
					$("#myModal input:last").val($(this).find("> span").attr("title"));
				} else {
					$("#myModal input").val("");
				}
				activeBox = $(this);
				return true;
			}).appendTo($("#ImageBox"));
		});

		$("#SaveBtn").click(function(){
			if($("#myModal input:first").val() !== "") {
				activeBox.find("> span").html($("#myModal input:first").val()).attr("title",$("#myModal input:last").val());
			} else {
				activeBox.find("> span").html(initMsg).removeAttr("title");
			}
			return true;
		});

		$("#DeleteBtn").click(function(){
			activeBox.remove();
			return true;
		});

		$("#Complete").click(function(){
			var forHomepage = '<div style="position:relative;float:left;max-width:100%;"><img src="<?=$uploaded_file?>"><div style="position:absolute;width:100%;height:100%;top:0;left:0;background:black;filter:alpha(opacity=0);opacity:0;"></div>';
			var forEmail = '<img src="<?=$uploaded_file?>" usemap="#imagemap"><map name="imagemap">';
			$("#ImageBox").find("a").each(function() {
				var cTop = 100.0*parseInt($(this).css("top"))/$("#ImageBox").height();
				var cLeft = 100.0*parseInt($(this).css("left"))/$("#ImageBox").width();
				var cWidth = 100.0*$(this).width()/$("#ImageBox").width();
				var cHeight = 100.0*$(this).height()/$("#ImageBox").height();

				forHomepage += '<a href="'+$(this).find("span").html()+'" style="background:black;filter:alpha(opacity=0);opacity:0;position:absolute;top:'+cTop+'%;left:'+cLeft+'%;width:'+cWidth+'%;height:'+cHeight+'%;" title="'+$(this).find("span").attr("title")+'"></a>';
				forEmail += '<area target=_blank shape="rect" coords="'+parseInt($(this).css("left"))+','+parseInt($(this).css("top"))+','+(parseInt($(this).css("left"))+$(this).width())+','+(parseInt($(this).css("top"))+$(this).height())+'" href="'+$(this).find("span").html()+'" alt="'+$(this).find("span").attr("title")+'">';
			});
			forHomepage += '</div><div style="clear:both;"></div><div><br></div>';
			forEmail += '</map>';
			$("#completeModal").find("textarea#forHomepage").val(forHomepage);
			$("#completeModal").find("textarea#forEmail").val(forEmail);
			return true;
		});

		$("#FileUploadBtn").click(function(){
			if($(this).parents("form").find("input").val().indexOf(" ") > 0) {
				alert("파일명에 공백을 없앤 후에 업로드해주세요.");
				return false;
			} else {
				return true;
			}
		});
	</script>
</body>
</html>

